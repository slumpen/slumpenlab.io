window.onload = function() {
    let wordlist = [];
    // We got it from here: https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-no-swears.txt
    fetch('/wordlist.txt').then(function (response) {
	    return response.text();
    }).then(function (text) {
        wordlist = text.split("\n");
        // Set the first password
        setNewPassword();
    }).catch(function (err) {
	    console.warn('Something went wrong.', err);
    });

    // Copy a bit from https://github.com/EFForg/OpenWireless/blob/master/app/js/diceware.js
    function Diceware() {
        if (typeof Math.log2 === 'undefined') {
            Math.log2 = function(N) {
                return Math.log(N)/Math.log(2) 
            };
        }
    }
        /**
     * Return a number of diceware words
     * @param int words - the number of words to generate
     */
    Diceware.prototype.getWords = function(words) {
        if (!window.crypto || !window.crypto.getRandomValues) {
            return [];
        }
        let diced = [];
        
        for (let i = 0; i < words; ++i) {
            diced.push(this.getSingleWord());
        }
        return diced;
    };
    
    // http://stackoverflow.com/questions/18230217/javascript-generate-a-random-number-within-a-range-using-crypto-getrandomvalues 
    
    Diceware.prototype.random = function (min, max) {
        let rval = 0;
        let range = max - min;
        
        let bits_needed = Math.ceil(Math.log2(range));
        if (bits_needed > 53) {
            throw new Exception("We cannot generate numbers larger than 53 bits.");
        }
        let bytes_needed = Math.ceil(bits_needed / 8);
        let mask = Math.pow(2, bits_needed) - 1; 
        // 7776 -> (2^13 = 8192) -1 == 8191 or 0x00001111 11111111
        
        // Create byte array and fill with N random numbers
        let byteArray = new Uint8Array(bytes_needed);
        window.crypto.getRandomValues(byteArray);
        
        let p = (bytes_needed - 1) * 8;
        for(let i = 0; i < bytes_needed; i++ ) {
            rval += byteArray[i] * Math.pow(2, p);
            p -= 8;   
        }
        
        // Use & to apply the mask and reduce the number of recursive lookups
        rval = rval & mask;
        
        if (rval >= range) {
            // Integer out of acceptable range
            return this.random(min, max);
        }
        // Return an integer that falls within the range
        return min + rval;
    }
    
    Diceware.prototype.getSingleWord = function() {
        if (!window.crypto || !window.crypto.getRandomValues) {
            return null;
        }
        let index = this.random(0, wordlist.length);
        return wordlist[index];
    };
    
    // function getRandomInteger(min, max) {
    //     return Math.floor(Math.random() * (max - min + 1) ) + min;
    // }
    
    function getRandomPassword() {
        let password = "";
        for (let i = 0; i < 5; i++) {
            // let word = wordlist[getRandomInteger(0, wordlist.length-1)];
            let word = Diceware.prototype.getSingleWord();
            word = word.charAt(0).toUpperCase() + word.slice(1);
            password += word;
        }
        password += "2!";
        return password;
    }
    function setNewPassword() {
        let password = getRandomPassword();
        document.getElementById("txt").value = password;
        navigator.clipboard.writeText(password);
    }

    document.getElementById("btn-generate").addEventListener("click", function() {
        setNewPassword();
    });
    
    document.getElementById("btn-copy").addEventListener("click", function() {
        let password = document.getElementById("txt").value;
        navigator.clipboard.writeText(password);
    });
};